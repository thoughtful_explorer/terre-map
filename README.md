# Terre Location Extractor
Extracts Terre (Belgian second hand / thrift store) locations from the official website, geocodes them, and places them into KML format.

## Dependencies	
* Python 3.x
    * Requests module for session functionality
    * Beautiful Soup 4.x (bs4) for scraping
    * Geocoder for retrieving geographic coordinates for addresses from OpenStreetMap's Nominatim service
    * Simplekml for easily building KML files
* Also of course depends on official [Terre](https://terre.be/magasins) website.
