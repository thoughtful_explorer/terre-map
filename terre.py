#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import simplekml
import geocoder
import re

#Set filename/path for KML file output
kmlfile = "terre.kml"
#Set KML schema name
kmlschemaname = "terre"
#Set page URL
pageURL = "https://www.terre.be/magasins"

#Reusable BeautifulSoup return of a given url (defaults to first store list page)
def getsoupinit(url=pageURL):
    #Start a session with the given page URL
    session = requests.Session()
    page = session.get(url,verify=False)
    #Return the soupified HTML
    return BeautifulSoup(page.content, 'html.parser')

#Returns a list of urls, each associated with one page of (unfortunately and hopelessly paginated) store locations. This is in case they suddenly get enough new stores to increase the pagination
def getstorepages():
    #Get soup of default URL
    soup = getsoupinit()
    #Initialize empty link list
    linklist = []
    #Get every page-link item except the last two (prev and next links)...
    for link in soup(class_="page-link")[:-2]:
        #...then iterate to concatenate the first part of the URL with the value of the href attributes and add it all to the linklist array
        linklist.append(pageURL+link.get('href'))
    return linklist

#Returns a list of divs that contain all information about each store
def getstores():
    #Initialize empty store list
    storelist = []
    #For each of the paginated store listing pages...
    for link in getstorepages():
        #...soupify the page...
        soup = getsoupinit(link)
        #...and then for each of the stores listed in that page..
        for div in soup(class_="store__item"):
            #...add the div container of that store into the storelist
            storelist.append(div)
    return storelist

#Extracts and returns only relevant store information for a given html store div
def getstore(storediv):
    #Get the name of the store from the page title
    storename = "Terre "+storediv.h3.get_text()
    #Get the address of the store from the first li and shed whitespace padding
    storeaddress = storediv.findAll('li')[0].get_text(strip=True)
    #Run the geocode
    geo = geocoder.osm(storeaddress)
    #Get coordinates from the geocode
    lat = geo.y
    lng = geo.x
    #Return all the goods
    return storename, storeaddress, lat, lng

#Saves a KML file of a given list of stores
def createkml():
    #Initialize kml objct
    kml = simplekml.Kml()
    #Add schema, which is required for a custom address field
    schema = kml.newschema(name=kmlschemaname)
    schema.newsimplefield(name="address",type="string")
    #Look through all the store urls
    for store in getstores():
        #Get all the returned variables from the getstore() function
        storename, storeaddress, lat, lng = getstore(store)
        #First, create the point name and description in the kml
        point = kml.newpoint(name=storename,description="thrift store")
        #Then, add the custom "SimpleData" address field to kml file with proper hierarchical kml data structure
        point.extendeddata.schemadata.schemaurl = kmlschemaname
        simpledata = point.extendeddata.schemadata.newsimpledata("address",storeaddress)
        #Finally, add coordinates to the feature
        point.coords=[(lng,lat)]
    #Save the final KML file
    kml.save(kmlfile)

createkml()
